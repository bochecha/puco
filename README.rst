*Note: This is an attempt at re-imagining the way we build and distribute
Ideascube. It is an experiment, and might lead nowhere. Do not rely on anything
here.*

====
Puco
====

This is a proof-of-concept tool for how we could distribute and run Ideascube.

Concepts
========

Puco revolves around two core concepts: applications and remotes.

Remotes
-------

A remote is a repository providing applications.

Puco uses `OSTree`_ under the hood, so a remote is just an OSTree repository
available at some URL.

Applications
------------

An application is a full tree containing the app executable and its
dependencies. The tree serves as the root (``/``) filesystem of a `Bubblewrap`_
container. How you build those trees is completely irrelevant to Puco.

Applications are references in the remote OSTree repository. That is, you must
commit the application tree into an OSTree repository. Application references
**must** be named following a certain convention:
``${id}/${architecture}/${branch}``.

A few examples would be:

* ``org.ideascube.Ideascube/x86_64/stable``
* ``org.ideascube.Ideascube/arm/devel``

(yes, those look like app references for `Flatpak`_; that is not a coincidence)

Whenever a command requires an application reference, passing the complete
id/arch/branch reference is preferred.

However Puco will try to guess the missing parts, following a certain heuristic:

* if the ``architecture`` part is missing, then it uses the local system
  architecture;
* if the ``branch`` part is missing, then it uses the word ``devel``;

That is, when trying to install the ``foobar`` application on a 64bits Intel
machine, Puco will automatically resolve that to ``foobar/x86_64/devel``.

To install the ``stable`` branch of the ``foobar`` application for the native
architecture of the machine, then you can use the ``foobar//stable`` shorthand.

Usage
=====

By default Puco will use ``/var/lib/puco`` as its working directory, and as
such needs the permissions to write there, typically by running with ``sudo``.

This default can be modified with the ``--puco-root`` argument. (potentially
removing the need to ``sudo``)

Managing remotes
----------------

Puco will let you install applications from repositories called "remotes".

You can add a remote::

    $ sudo puco remotes add REMOTE_NAME REMOTE_URL

You can then list the configured remotes (the following two are equivalent)::

    $ sudo puco remotes list
    $ sudo puco remotes

And you can delete a remote, provided no apps were installed from it::

    $ sudo puco remotes delete REMOTE_NAME

Managing applications
---------------------

You can list applications available in a remote::

    $ sudo puco remotes list-apps REMOTE_NAME

You can then install an application::

    $ sudo puco install REMOTE_NAME APPLICATION_REF

You can list installed applications::

    $ sudo puco list

You can update an application::

    $ sudo puco update APPLICATION_REF

And you can uninstall an application::

    $ sudo puco remove APPLICATION_REF

Running applications
--------------------

Puco leverages Bubblewrap to run application containers.


You can run them with the following command::

    $ puco run APPLICATION_REF CMD

The command to run can be any number or arguments; for example you could run a
Pyramid application as follows::

    $ puco run APPLICATION_REF pserve /path/to/config.ini


.. _Bubblewrap: https://github.com/projectatomic/bubblewrap
.. _Flatpak: https://flatpak.org
.. _OSTree: https://ostree.readthedocs.io/en/latest/
