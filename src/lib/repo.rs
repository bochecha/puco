/* Copyright (c) 2017 - Mathieu Bridon <bochecha@daitauha.fr>
 *
 * This file is part of Puco
 *
 * Puco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Puco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Puco.  If not, see <http://www.gnu.org/licenses/>.
 */


use std::fs::{File, create_dir_all, remove_dir_all, rename};
use std::io::prelude::*;
use std::os::unix::fs::symlink;
use std::path::{Path, PathBuf};
use std::process::Command;

use failure::Error;

use glob::glob;

use toml;

use url::Url;

use super::errors::PucoError;
use super::utils::get_id_arch_branch_tuple;

#[cfg(test)]
#[path = "./test_repo.rs"]
mod test_repo;


pub struct Repo {
    root_dir: PathBuf,
    repo_dir: PathBuf,
    apps_dir: PathBuf,
}


impl Repo {
    fn run_ostree_command(&self, args: &[&str]) -> Result<String, Error> {
        let repo_path = match self.repo_dir.to_str() {
            Some(path) => path,
            None => {
                bail!(
                    "Invalid repository path: {}",
                    self.repo_dir.to_string_lossy()
                )
            }
        };
        let mut command = Command::new("ostree");
        let command = command.arg(format!("--repo={}", repo_path));
        let command = command.args(args);
        let output = command.output()?;

        if !output.status.success() {
            let cmd = format!("{:?}", command);
            return Err(Error::from(PucoError::CommandError(cmd, output)));
        }

        Ok(String::from(String::from_utf8_lossy(&output.stdout)))
    }

    pub fn new(root_dir: &Path) -> Result<Repo, Error> {
        let root_dir = root_dir.canonicalize()?;
        let repo_dir = root_dir.join("repo");
        let apps_dir = root_dir.join("apps");

        let repo = Repo {
            root_dir: root_dir,
            repo_dir: repo_dir,
            apps_dir: apps_dir,
        };
        repo.run_ostree_command(&["init", "--mode=bare-user"])?;

        Ok(repo)
    }

    /* ---------------------------------------------------------------------
     *                Manage remotes
     * --------------------------------------------------------------------- */

    fn get_remote(&self, name: &str) -> Result<Remote, Error> {
        for remote in self.list_remotes()? {
            if remote.name == name {
                return Ok(remote);
            }
        }

        bail!("Could not find a configured remote named \"{}\"", name)
    }

    fn remote_is_known(&self, name: &str) -> Result<bool, Error> {
        for remote in self.list_remotes()? {
            if remote.name == name {
                return Ok(true);
            }
        }

        Ok(false)
    }

    pub fn add_remote(&self, name: &str, url: &Url) -> Result<(), Error> {
        if self.remote_is_known(name)? {
            bail!("A remote named \"{}\" already exists", name)
        }

        // TODO: Enable GPG
        // TODO: Maybe something similar to .flatpakrepo files?
        self.run_ostree_command(
            &[
                "remote",
                "add",
                "--no-gpg-verify",
                name,
                url.as_str(),
            ],
        )?;

        Ok(())
    }

    pub fn list_apps_in_remote(&self, remote_name: &str) -> Result<Vec<App>, Error> {
        self.get_remote(remote_name)?;

        let output = self.run_ostree_command(&["remote", "refs", remote_name])?;
        let mut apps = Vec::new();

        for line in output.lines() {
            if line.is_empty() {
                continue;
            }

            let mut split = line.split(":");
            let remote_name = split.nth(0).unwrap();
            let app_ref = split.nth(0).unwrap();

            self.run_ostree_command(
                &[
                    "pull",
                    "--commit-metadata-only",
                    remote_name,
                    &app_ref,
                ],
            )?;
            let commit = self.run_ostree_command(&["rev-parse", &app_ref])?
                .trim()
                .to_owned();

            let v: Vec<&str> = app_ref.split("/").collect();

            apps.push(App {
                id: v[0].to_owned(),
                arch: v[1].to_owned(),
                branch: v[2].to_owned(),
                remote_name: remote_name.to_owned(),
                commit: commit,
            });
        }

        Ok(apps)
    }

    pub fn list_remotes(&self) -> Result<Vec<Remote>, Error> {
        let output = self.run_ostree_command(&["remote", "list", "--show-urls"])?;
        let mut remotes = Vec::new();

        for line in output.lines() {
            if line.is_empty() {
                continue;
            }

            let v: Vec<&str> = line.split("  ").collect();

            remotes.push(Remote {
                name: v[0].to_owned(),
                url: Url::parse(v[1])?,
            });
        }

        Ok(remotes)
    }

    pub fn remove_remote(&self, name: &str) -> Result<(), Error> {
        self.get_remote(name)?;
        let apps = self.list_installed_apps()?;
        let apps_from_remote = apps.into_iter()
            .filter(|ref app| app.remote_name == name)
            .collect::<Vec<_>>();

        if apps_from_remote.len() > 0 {
            let mut msg = format!(
                "Cannot remove the remote \"{}\"; some apps were installed from it:\n\n",
                name
            );

            for app in apps_from_remote {
                msg.push_str(&format!("- {}", app.as_refspec()));
            }

            bail!(msg)
        }

        self.run_ostree_command(&["remote", "delete", name])?;

        Ok(())
    }

    /* ---------------------------------------------------------------------
     *                Manage apps
     * --------------------------------------------------------------------- */

    fn get_app_path(&self, app: &App) -> PathBuf {
        let app_ref = app.as_refspec();
        self.apps_dir.join(&app_ref).join(&app.commit)
    }

    pub fn get_app_checkout_path(&self, app: &App) -> PathBuf {
        self.get_app_path(app).join("files")
    }

    pub fn get_installed_app(&self, app_ref: &str) -> Result<App, Error> {
        let (id, arch, branch) = get_id_arch_branch_tuple(app_ref)?;

        for app in self.list_installed_apps()? {
            if app.id == id && app.branch == branch && app.arch == arch {
                return Ok(app);
            }
        }

        bail!("{} is not installed", app_ref)
    }

    fn get_remote_app(&self, remote: Remote, app_ref: &str) -> Result<App, Error> {
        let (id, arch, branch) = get_id_arch_branch_tuple(app_ref)?;

        for app in self.list_apps_in_remote(&remote.name)? {
            if app.id == id && app.arch == arch && app.branch == branch {
                return Ok(app);
            }
        }

        bail!("Could not find {} in {}", app_ref, remote.name)
    }

    fn is_installed(&self, app: &App) -> Result<bool, Error> {
        for installed in self.list_installed_apps()? {
            if app.id == installed.id && app.branch == installed.branch &&
                app.arch == installed.arch
            {
                return Ok(true);
            }
        }

        Ok(false)
    }

    fn pull(&self, app: &App) -> Result<(), Error> {
        let app_ref = app.as_refspec();

        // TODO: Display download progress
        self.run_ostree_command(
            &["pull", &app.remote_name, &app_ref],
        )?;

        Ok(())
    }

    fn checkout(&self, app: &App) -> Result<(), Error> {
        let app_path = self.get_app_path(app);
        create_dir_all(&app_path)?;
        let checkout_path = self.get_app_checkout_path(app);
        let parent = app_path.parent().unwrap();

        let checkout_dir = match checkout_path.to_str() {
            Some(path) => path,
            None => {
                bail!(
                    "Invalid app checkout dir: {}",
                    checkout_path.to_string_lossy()
                )
            }
        };
        self.run_ostree_command(
            &[
                "checkout",
                "--user-mode",
                "--require-hardlinks",
                &app.as_refspec(),
                checkout_dir,
            ],
        )?;

        // Set the "active" symlink
        let active_path = parent.join("active");
        let tmp_active_path = parent.join(".active-temp-link");
        symlink(&app.commit, &tmp_active_path)?;
        rename(&tmp_active_path, &active_path)?;

        Ok(())
    }

    fn deploy(&self, app: &App) -> Result<(), Error> {
        let app_path = self.get_app_path(app);
        let deployed_path = app_path.join("deployed");

        let serialized = app.serialize()?;
        let serialized = serialized.into_bytes();

        let mut file = File::create(&deployed_path)?;
        file.write_all(&serialized)?;

        Ok(())
    }

    fn get_deployed(&self, path: &Path) -> Result<App, Error> {
        let mut file = File::open(path)?;
        let mut serialized = String::new();

        file.read_to_string(&mut serialized)?;
        let app = App::from_serialized(&serialized)?;

        Ok(app)
    }

    pub fn install_app(&self, remote_name: &str, app_ref: &str) -> Result<(), Error> {
        let remote = self.get_remote(remote_name)?;
        let app = self.get_remote_app(remote, app_ref)?;

        if self.is_installed(&app)? {
            println!(
                "{} is already installed (from {})",
                app_ref,
                app.remote_name
            );
            return Ok(());
        }

        println!("Installing {} from {}...", app.as_refspec(), remote_name);
        self.pull(&app)?;
        self.checkout(&app)?;
        self.deploy(&app)?;

        Ok(())
    }

    pub fn list_installed_apps(&self) -> Result<Vec<App>, Error> {
        let path = self.apps_dir.join("*/*/*/active/deployed");
        let pattern = match path.to_str() {
            Some(s) => s,
            None => {
                bail!(
                    "Invalid repository path: {}",
                    self.repo_dir.to_string_lossy()
                )
            }
        };

        let mut apps = Vec::new();

        for path in glob(&pattern).unwrap().filter_map(Result::ok) {
            let app = self.get_deployed(&path)?;
            apps.push(app);
        }

        Ok(apps)
    }

    pub fn remove_app(&self, app_ref: &str) -> Result<(), Error> {
        let app = self.get_installed_app(app_ref)?;

        // Get the full refspec, in case a shorthand had been specified
        let app_ref = app.as_refspec();

        // TODO: Should we be careful with running apps?

        println!("Removing {}...", &app_ref);

        let app_path = self.apps_dir.join(app_ref);
        remove_dir_all(&app_path)?;

        self.run_ostree_command(
            &["refs", "--delete", &app.as_remote_refspec()],
        )?;
        self.run_ostree_command(&["prune", "--refs-only"])?;

        Ok(())
    }

    pub fn update_app(&self, app_ref: &str) -> Result<(), Error> {
        let installed = self.get_installed_app(app_ref)?;
        let remote = self.get_remote(&installed.remote_name)?;

        // Get the full app ref, in case a shorthand had been specified
        let app_ref = installed.as_refspec();

        let available = self.get_remote_app(remote, &app_ref)?;
        if available.commit == installed.commit {
            println!("{} is already up to date", app_ref);
            return Ok(());
        }

        println!("Updating {}...", app_ref);
        self.pull(&available)?;
        self.checkout(&available)?;
        self.deploy(&available)?;

        // TODO: Should we be careful with running apps?
        let old_path = self.get_app_path(&installed);
        remove_dir_all(&old_path)?;

        Ok(())
    }
}


pub struct Remote {
    pub name: String,
    pub url: Url,
}


#[derive(Serialize, Deserialize)]
pub struct App {
    pub id: String,
    pub arch: String,
    pub branch: String,
    pub remote_name: String,
    pub commit: String,
}


impl App {
    pub fn as_refspec(&self) -> String {
        format!("{}/{}/{}", self.id, self.arch, self.branch)
    }

    pub fn as_remote_refspec(&self) -> String {
        format!(
            "{}:{}/{}/{}",
            self.remote_name,
            self.id,
            self.arch,
            self.branch
        )
    }

    fn serialize(&self) -> Result<String, Error> {
        Ok(toml::to_string(&self)?)
    }

    pub fn from_serialized(serialized: &str) -> Result<App, Error> {
        Ok(toml::from_str(serialized)?)
    }
}
