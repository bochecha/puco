/* Copyright (c) 2017 - Mathieu Bridon <bochecha@daitauha.fr>
 *
 * This file is part of Puco
 *
 * Puco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Puco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Puco.  If not, see <http://www.gnu.org/licenses/>.
 */


use super::*;


#[cfg(test)]
mod test_check_program {
    use super::*;

    #[test]
    fn existing_program() {
        assert!(check_program("cargo").is_ok());
    }

    #[test]
    fn program_not_found() {
        let res = check_program("nope-nope");
        assert!(res.is_err());

        let e = res.err().unwrap();
        assert_eq!(format!("{}", e), String::from("Could not find nope-nope"));
    }
}


#[cfg(test)]
mod test_get_id_arch_branch_tuple {
    use super::*;

    #[test]
    fn full_ref() {
        let (id, arch, branch) = get_id_arch_branch_tuple("org.puco.Puco/x86_64/stable").unwrap();
        assert_eq!(&id, "org.puco.Puco");
        assert_eq!(&arch, "x86_64");
        assert_eq!(&branch, "stable");
    }

    #[test]
    fn id_only() {
        let (id, arch, branch) = get_id_arch_branch_tuple("org.puco.Puco").unwrap();
        assert_eq!(&id, "org.puco.Puco");
        assert_eq!(&arch, "x86_64");
        assert_eq!(&branch, "devel");

        let (id, arch, branch) = get_id_arch_branch_tuple("org.puco.Puco/").unwrap();
        assert_eq!(&id, "org.puco.Puco");
        assert_eq!(&arch, "x86_64");
        assert_eq!(&branch, "devel");

        let (id, arch, branch) = get_id_arch_branch_tuple("org.puco.Puco//").unwrap();
        assert_eq!(&id, "org.puco.Puco");
        assert_eq!(&arch, "x86_64");
        assert_eq!(&branch, "devel");
    }

    #[test]
    fn missing_id() {
        let res = get_id_arch_branch_tuple("/x86_64/stable");
        assert!(res.is_err());
    }

    #[test]
    fn missing_arch() {
        let (id, arch, branch) = get_id_arch_branch_tuple("org.puco.Puco//stable").unwrap();
        assert_eq!(&id, "org.puco.Puco");
        assert_eq!(&arch, "x86_64");
        assert_eq!(&branch, "stable");
    }

    #[test]
    fn missing_branch() {
        let (id, arch, branch) = get_id_arch_branch_tuple("org.puco.Puco/x86_64").unwrap();
        assert_eq!(&id, "org.puco.Puco");
        assert_eq!(&arch, "x86_64");
        assert_eq!(&branch, "devel");

        let (id, arch, branch) = get_id_arch_branch_tuple("org.puco.Puco/x86_64/").unwrap();
        assert_eq!(&id, "org.puco.Puco");
        assert_eq!(&arch, "x86_64");
        assert_eq!(&branch, "devel");
    }

    #[test]
    fn empty() {
        let res = get_id_arch_branch_tuple("");
        assert!(res.is_err());

        let res = get_id_arch_branch_tuple("/");
        assert!(res.is_err());

        let res = get_id_arch_branch_tuple("//");
        assert!(res.is_err());
    }

    #[test]
    fn invalid() {
        let res = get_id_arch_branch_tuple("org.puco.Puco/x86_64/stable/");
        assert!(res.is_err());

        let res = get_id_arch_branch_tuple("great app/x86_64/stable");
        assert!(res.is_err());
    }
}


#[cfg(test)]
mod test_parse_url {
    use super::*;

    #[test]
    fn https() {
        let url = parse_url("https://example.com/foo/bar").unwrap();
        assert_eq!(url.scheme(), "https");
        assert_eq!(url.host_str(), Some("example.com"));
        assert_eq!(url.path(), "/foo/bar");
    }

    #[test]
    fn file() {
        let url = parse_url("file:///foo/bar").unwrap();
        assert_eq!(url.scheme(), "file");
        assert_eq!(url.host_str(), None);
        assert_eq!(url.path(), "/foo/bar");
    }

    #[test]
    fn path_absolute() {
        let url = parse_url("/foo/bar").unwrap();
        assert_eq!(url.scheme(), "file");
        assert_eq!(url.host_str(), None);
        assert_eq!(url.path(), "/foo/bar");
    }

    #[test]
    fn path_relative() {
        let mut expected_path = env::current_dir().unwrap();
        expected_path.push("foo/bar");

        let url = parse_url("foo/bar").unwrap();
        assert_eq!(url.scheme(), "file");
        assert_eq!(url.host_str(), None);
        assert_eq!(url.path(), expected_path.to_string_lossy());
    }

    #[test]
    fn path_relative_dot() {
        let mut expected_path = env::current_dir().unwrap();
        expected_path.push("foo/bar");

        let url = parse_url("./foo/bar").unwrap();
        assert_eq!(url.scheme(), "file");
        assert_eq!(url.host_str(), None);
        assert_eq!(url.path(), expected_path.to_string_lossy());
    }

    #[test]
    fn path_relative_dot_dot() {
        let mut expected_path = env::current_dir().unwrap();
        expected_path.pop();
        expected_path.push("foo/bar");

        let url = parse_url("../foo/bar").unwrap();
        assert_eq!(url.scheme(), "file");
        assert_eq!(url.host_str(), None);
        assert_eq!(url.path(), expected_path.to_string_lossy());
    }
}
