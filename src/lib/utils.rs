/* Copyright (c) 2017 - Mathieu Bridon <bochecha@daitauha.fr>
 *
 * This file is part of Puco
 *
 * Puco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Puco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Puco.  If not, see <http://www.gnu.org/licenses/>.
 */


use std::env;
use std::fmt;
use std::path::PathBuf;

use console::style;

use failure::Error;

use url;

#[cfg(test)]
#[path = "./test_utils.rs"]
mod test_utils;


pub fn check_program(name: &str) -> Result<(), Error> {
    let path = env::var("PATH")?;
    let dirs = path.split(":");

    for dir in dirs {
        let path = PathBuf::from(dir).join(name);
        if path.exists() {
            return Ok(());
        }
    }

    bail!("Could not find {}", name);
}


pub fn print_header(msg: &fmt::Display) {
    println!("{}", style(msg).bold());
}


pub fn print_err(msg: &fmt::Display) {
    eprintln!("{} {}", style("Error:").bold().red(), msg);
}


pub fn get_arch() -> String {
    if cfg!(target_arch = "x86_64") {
        String::from("x86_64")
    } else {
        panic!("Unknown local architecture, please report a bug");
    }
}


pub fn get_id_arch_branch_tuple(refspec: &str) -> Result<(String, String, String), Error> {
    let refspec = refspec.trim();

    if refspec.contains(" ") {
        bail!("Could not parse {}", refspec);
    }

    let v: Vec<&str> = refspec.split('/').collect();
    let id = String::from(v[0]);

    if id == "" {
        bail!("Could not parse {}", refspec);
    }

    let default_arch = get_arch();
    let default_branch = String::from("devel");

    match v.len() {
        1 => {
            return Ok((id, default_arch, default_branch));
        }
        2 => {
            if v[1] == "" {
                return Ok((id, default_arch, default_branch));
            } else {
                return Ok((id, String::from(v[1]), default_branch));
            }
        }
        3 => {
            if v[1] == "" && v[2] == "" {
                return Ok((id, default_arch, default_branch));
            } else if v[1] == "" {
                return Ok((id, default_arch, String::from(v[2])));
            } else if v[2] == "" {
                return Ok((id, String::from(v[1]), default_branch));
            } else {
                return Ok((id, String::from(v[1]), String::from(v[2])));
            }
        }
        _ => {
            bail!("Could not parse {}", refspec);
        }
    };
}


pub fn parse_url(url: &str) -> Result<url::Url, Error> {
    let url = match url::Url::parse(url) {
        Ok(parsed) => parsed,
        Err(url::ParseError::RelativeUrlWithoutBase) => {
            // This was a local path
            let mut path = env::current_dir()?;
            path.push(url);
            let path = match path.to_str() {
                Some(path) => path,
                None => {
                    return Err(format_err!("Invalid path: {}", path.to_string_lossy()));
                }
            };
            url::Url::parse(&format!("file://{}", path))?
        }
        Err(e) => {
            return Err(Error::from(e));
        }
    };

    Ok(url)
}
