/* Copyright (c) 2017 - Mathieu Bridon <bochecha@daitauha.fr>
 *
 * This file is part of Puco
 *
 * Puco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Puco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Puco.  If not, see <http://www.gnu.org/licenses/>.
 */


use std::fs::{File, create_dir_all, remove_dir_all, rename};
use std::str;

use super::*;


#[cfg(test)]
mod test_new_repo {
    use super::*;

    #[test]
    fn success() {
        let root_dir = Path::new("./test-workdirs/test_new_repo/success");
        create_dir_all(root_dir).unwrap();

        let expected_root_dir = root_dir.canonicalize().unwrap();
        let expected_repo_dir = expected_root_dir.join("repo");

        let repo = Repo::new(root_dir).unwrap();
        assert_eq!(repo.root_dir, expected_root_dir);
        assert_eq!(repo.repo_dir, expected_repo_dir);

        // FIXME: Should we assume knowledge of the layout of an OSTree repo?
        assert!(expected_repo_dir.join("config").is_file());
        assert!(expected_repo_dir.join("objects").is_dir());

        remove_dir_all(root_dir).unwrap();
    }

    #[test]
    fn missing_parent() {
        let root_dir = Path::new("./test-workdirs/test_new_repo/missing_parent");

        let res = Repo::new(root_dir);
        assert!(res.is_err());

        let e = res.err().unwrap();
        assert!(format!("{}", e).contains("No such file or directory"));
    }

    #[test]
    fn permission_denied() {
        let root_dir = Path::new("/usr");

        let res = Repo::new(root_dir);
        assert!(res.is_err());

        let e = res.err().unwrap();
        assert!(format!("{}", e).contains("Permission denied"));
    }
}


#[cfg(test)]
mod test_add_remote {
    use super::*;

    #[test]
    fn success() {
        let root_dir = Path::new("./test-workdirs/test_add_remote/success");
        create_dir_all(root_dir).unwrap();
        let repo = Repo::new(root_dir).unwrap();

        let url = Url::parse("https://example.com/foo/bar").unwrap();
        let res = repo.add_remote("test", &url);
        assert!(res.is_ok());

        // TODO: Can we test some more?

        remove_dir_all(root_dir).unwrap();
    }

    #[test]
    fn twice_fails() {
        let root_dir = Path::new("./test-workdirs/test_add_remote/twice_fails");
        create_dir_all(root_dir).unwrap();
        let repo = Repo::new(root_dir).unwrap();

        let url = Url::parse("https://example.com/foo/bar").unwrap();
        repo.add_remote("test", &url).unwrap();

        let res = repo.add_remote("test", &url);
        assert!(res.is_err());

        let e = res.err().unwrap();
        assert_eq!(format!("{}", e), "A remote named \"test\" already exists");

        remove_dir_all(root_dir).unwrap();
    }

    #[test]
    fn invalid_name() {
        let root_dir = Path::new("./test-workdirs/test_add_remote/invalid_name");
        create_dir_all(root_dir).unwrap();
        let repo = Repo::new(root_dir).unwrap();

        let url = Url::parse("https://example.com/foo/bar").unwrap();
        let res = repo.add_remote("", &url);
        assert!(res.is_err());

        let e = res.err().unwrap();
        let e = e.downcast::<PucoError>().unwrap();

        match e {
            PucoError::CommandError(ref cmd, ref output) => {
                assert!(cmd.contains(
                    "\"remote\" \"add\" \"--no-gpg-verify\" \"\" \"https://example.com/foo/bar\"",
                ));

                let stderr = str::from_utf8(&output.stderr).unwrap();
                let expected_stderr = "error: Invalid remote name \n";
                assert_eq!(stderr, expected_stderr);
            }
        };

        remove_dir_all(root_dir).unwrap();
    }
}


#[cfg(test)]
mod test_list_apps_in_remote {
    use super::*;

    #[test]
    fn success() {
        let root_dir = Path::new("./test-workdirs/test_list_apps_in_remote/success");
        create_dir_all(root_dir).unwrap();

        // First setup a repo to use as a remote
        let root_dir = root_dir.canonicalize().unwrap();
        let repo = Repo::new(&root_dir).unwrap();

        // Commit something to this repo
        let tmp_dir = root_dir.join("tmp");
        let tmp_file = tmp_dir.join("foo");
        create_dir_all(&tmp_dir).unwrap();
        File::create(tmp_file).unwrap();
        repo.run_ostree_command(
            &[
                "commit",
                "--canonical-permissions",
                "--branch=org.puco.Puco/x86_64/stable",
                tmp_dir.to_str().unwrap(),
            ],
        ).unwrap();
        remove_dir_all(tmp_dir).unwrap();

        // Ask OSTree to add a summary file (it contains the list of apps)
        repo.run_ostree_command(&["summary", "--update"]).unwrap();

        // Now move the remote repo; It cannot be used from now on!
        let remote_dir = root_dir.join("_remote");
        rename(repo.repo_dir, &remote_dir).unwrap();

        let repo = Repo::new(&root_dir).unwrap();
        let remote_url = &format!("file://{}", &remote_dir.to_str().unwrap());
        let url = Url::parse(remote_url).unwrap();
        repo.add_remote("test", &url).unwrap();

        let apps = repo.list_apps_in_remote("test").unwrap();
        assert_eq!(apps.len(), 1);
        assert_eq!(apps[0].id, "org.puco.Puco");
        assert_eq!(apps[0].arch, "x86_64");
        assert_eq!(apps[0].branch, "stable");
        assert_eq!(apps[0].remote_name, "test");

        remove_dir_all(root_dir).unwrap();
    }

    #[test]
    fn empty_remote() {
        let root_dir = Path::new("./test-workdirs/test_list_apps_in_remote/empty_remote");
        create_dir_all(root_dir).unwrap();

        // First setup a repo to use as a remote
        let root_dir = root_dir.canonicalize().unwrap();
        let repo = Repo::new(&root_dir).unwrap();

        // Ask OSTree to add a summary file (it contains the list of apps)
        repo.run_ostree_command(&["summary", "--update"]).unwrap();

        // Now move the remote repo; It cannot be used from now on!
        let remote_dir = root_dir.join("_remote");
        rename(repo.repo_dir, &remote_dir).unwrap();

        let repo = Repo::new(&root_dir).unwrap();
        let remote_url = &format!("file://{}", &remote_dir.to_str().unwrap());
        let url = Url::parse(remote_url).unwrap();
        repo.add_remote("test", &url).unwrap();

        let apps = repo.list_apps_in_remote("test").unwrap();
        assert_eq!(apps.len(), 0);

        remove_dir_all(root_dir).unwrap();
    }

    #[test]
    fn no_such_remote() {
        let root_dir = Path::new("./test-workdirs/test_list_apps_in_remote/no_such_remote");
        create_dir_all(root_dir).unwrap();
        let repo = Repo::new(&root_dir).unwrap();

        let res = repo.list_apps_in_remote("test");
        assert!(res.is_err());

        let e = res.err().unwrap();
        assert_eq!(
            format!("{}", e),
            "Could not find a configured remote named \"test\""
        );

        remove_dir_all(root_dir).unwrap();
    }
}


#[cfg(test)]
mod test_list_remotes {
    use super::*;

    #[test]
    fn success() {
        let root_dir = Path::new("./test-workdirs/test_list_remotes/success");
        create_dir_all(root_dir).unwrap();
        let repo = Repo::new(root_dir).unwrap();
        let url = Url::parse("https://example.com/foo/bar").unwrap();
        repo.add_remote("test", &url).unwrap();

        let remotes = repo.list_remotes().unwrap();
        assert_eq!(remotes.len(), 1);
        assert_eq!(remotes[0].name, "test");
        assert_eq!(remotes[0].url, url);

        remove_dir_all(root_dir).unwrap();
    }

    #[test]
    fn no_remotes() {
        let root_dir = Path::new("./test-workdirs/test_list_remotes/no_remotes");
        create_dir_all(root_dir).unwrap();
        let repo = Repo::new(root_dir).unwrap();

        let remotes = repo.list_remotes().unwrap();
        assert_eq!(remotes.len(), 0);

        remove_dir_all(root_dir).unwrap();
    }
}


#[cfg(test)]
mod test_remove_remotes {
    use super::*;

    #[test]
    fn success() {
        let root_dir = Path::new("./test-workdirs/test_remove_remote/success");
        create_dir_all(root_dir).unwrap();
        let repo = Repo::new(root_dir).unwrap();
        let url = Url::parse("https://example.com/foo/bar").unwrap();

        repo.add_remote("test", &url).unwrap();
        let remotes = repo.list_remotes().unwrap();
        assert_eq!(remotes.len(), 1);

        repo.remove_remote("test").unwrap();
        let remotes = repo.list_remotes().unwrap();
        assert_eq!(remotes.len(), 0);

        remove_dir_all(root_dir).unwrap();
    }

    #[test]
    fn no_such_remote() {
        let root_dir = Path::new("./test-workdirs/test_remove_remote/no_such_remote");
        create_dir_all(root_dir).unwrap();
        let repo = Repo::new(root_dir).unwrap();

        let remotes = repo.list_remotes().unwrap();
        assert_eq!(remotes.len(), 0);

        let res = repo.remove_remote("test");
        assert!(res.is_err());

        let e = res.err().unwrap();
        assert_eq!(
            format!("{}", e),
            "Could not find a configured remote named \"test\""
        );

        remove_dir_all(root_dir).unwrap();
    }
}
