/* Copyright (c) 2017 - Mathieu Bridon <bochecha@daitauha.fr>
 *
 * This file is part of Puco
 *
 * Puco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Puco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Puco.  If not, see <http://www.gnu.org/licenses/>.
 */


use std::fmt;
use std::process::Output;


#[derive(Fail, Debug)]
pub enum PucoError {
    CommandError(String, Output),
}


impl fmt::Display for PucoError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            &PucoError::CommandError(ref cmd, ref output) => {
                let stdout = String::from_utf8_lossy(&output.stdout);
                let stderr = String::from_utf8_lossy(&output.stderr);
                let mut s = format!("Failed to run command:\n  {}\n\n", &cmd);

                if !stdout.is_empty() {
                    s.push_str(&format!("== Captured stdout ==\n{}\n\n", stdout));
                }

                if !stderr.is_empty() {
                    s.push_str(&format!("== Captured stderr ==\n{}\n\n", stderr));
                }

                write!(f, "{}", s)
            }
        }
    }
}
