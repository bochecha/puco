/* Copyright (c) 2017 - Mathieu Bridon <bochecha@daitauha.fr>
 *
 * This file is part of Puco
 *
 * Puco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Puco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Puco.  If not, see <http://www.gnu.org/licenses/>.
 */


use std::path::PathBuf;
use std::process::Command;

use failure::Error;


pub struct Sandbox {
    app_id: String,
    app_path: PathBuf,
}

impl Sandbox {
    pub fn new(app_id: String, app_path: PathBuf) -> Sandbox {
        Sandbox { app_id, app_path }
    }

    pub fn run(&self, cmd: &Vec<String>) -> Result<(), Error> {
        let app_path = self.app_path.to_str().unwrap();

        let mut command = Command::new("bwrap");
        let command = command.args(
            &[
                "--bind",
                app_path,
                "/",

                "--dev",
                "/dev",

                "--ro-bind",
                "/etc/resolv.conf",
                "/etc/resolv.conf",

                "--ro-bind",
                "/etc/hosts",
                "/etc/hosts",

                "--ro-bind",
                "/etc/nsswitch.conf",
                "/etc/nsswitch.conf",

                "--unshare-all",
                "--share-net",

                "--die-with-parent",
            ],
        );

        // TODO: Sanitize environment variables

        let config_path = PathBuf::from(format!("/etc/{}", self.app_id));
        let command = match config_path.is_dir() {
            false => command,
            true => {
                let config_dir = config_path.to_str().unwrap();
                command.args(&["--ro-bind", config_dir, config_dir])
            }
        };

        let state_path = PathBuf::from(format!("/var/lib/{}", self.app_id));
        let command = match state_path.is_dir() {
            false => command,
            true => {
                let state_dir = state_path.to_str().unwrap();
                command.args(&["--bind", state_dir, state_dir])
            }
        };

        let command = command.args(&["--remount-ro", "/"]);

        let command = command.args(cmd);
        command.spawn()?.wait()?;

        Ok(())
    }
}
