/* Copyright (c) 2017 - Mathieu Bridon <bochecha@daitauha.fr>
 *
 * This file is part of Puco
 *
 * Puco is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Puco is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Puco.  If not, see <http://www.gnu.org/licenses/>.
 */


extern crate console;
#[macro_use]
extern crate failure;
extern crate glob;
#[macro_use]
extern crate serde_derive;
extern crate structopt;
#[macro_use]
extern crate structopt_derive;
extern crate toml;
extern crate url;

use failure::Error;

use structopt::StructOpt;

use std::fs::create_dir_all;
use std::path::Path;

mod lib;


#[derive(StructOpt)]
#[structopt(name = "puco")]
/// Run containers.
struct Cli {
    #[structopt(long = "puco-root", default_value = "/var/lib/puco")]
    /// The Puco root directory
    puco_root: String,

    #[structopt(subcommand)]
    cmd: Command,
}

#[derive(StructOpt)]
enum Command {
    #[structopt(name = "install")]
    /// Install an application
    Install {
        /// The name of the remote from which to install the app
        remote_name: String,

        /// The name of the application to install
        app_ref: String,
    },

    #[structopt(name = "list")]
    /// List installed applications
    List,

    #[structopt(name = "remove")]
    /// Remove an installed application
    Remove {
        /// The name of the application to remove
        app_ref: String,
    },

    #[structopt(name = "update")]
    /// Update an application
    Update {
        /// The name of the application to update
        app_ref: String,
    },

    #[structopt(name = "run")]
    /// Run applications
    Run {
        /// The name of the application to run
        app_ref: String,

        /* FIXME: We can't simply use the obvious:
         *
         * #[structopt(required = false)]
         *
         * This is due to the following issue:
         * https://github.com/rust-lang/rust/issues/34981
         *
         * I don't really want to depend on a nightly compiler just for that,
         * though.
         */
        /// The (optional) command to run
        cmd: Vec<String>,
    },

    #[structopt(name = "remotes")]
    /// Manage remotes
    Remotes {
        #[structopt(subcommand)]
        cmd: Option<Remotes>,
    },
}

#[derive(StructOpt)]
enum Remotes {
    #[structopt(name = "add")]
    /// Add a remote
    Add {
        /// The desired name for the new remote
        name: String,

        #[structopt(parse(try_from_str = "lib::utils::parse_url"))]
        /// The URL for the remote
        url: url::Url,
    },

    #[structopt(name = "list")]
    /// List the configured remotes
    List,

    #[structopt(name = "list-apps")]
    /// List the apps in a remote
    ListApps {
        /// The name of the remote from which to list apps
        name: String,
    },

    #[structopt(name = "remove")]
    /// Remove a configured remote
    Remove {
        /// The name of the remote to remove
        name: String,
    },
}


fn main() {
    let exit_code = match real_main() {
        Ok(()) => 0,
        Err(e) => {
            lib::utils::print_err(&e);
            1
        }
    };
    std::process::exit(exit_code);
}


fn real_main() -> Result<(), Error> {
    lib::utils::check_program("ostree")?;

    let args = Cli::from_args();

    let root_dir = Path::new(&args.puco_root);
    create_dir_all(&root_dir)?;
    let repo = lib::repo::Repo::new(root_dir)?;

    match args.cmd {
        Command::Remotes { cmd } => {
            match cmd {
                Some(Remotes::Add { name, url }) => {
                    repo.add_remote(&name, &url)?;
                }
                Some(Remotes::ListApps { name }) => {
                    let apps = repo.list_apps_in_remote(&name)?;

                    if apps.len() != 0 {
                        lib::utils::print_header(&format!("{:<45} {}", "Application", "Commit"));

                        for app in apps {
                            println!(
                                "{:<45} {}",
                                app.as_refspec(),
                                app.commit.get(0..10).unwrap()
                            );
                        }
                    }
                }
                Some(Remotes::Remove { name }) => {
                    repo.remove_remote(&name)?;
                }
                _ => {
                    // This covers both Some(Remotes::List) and None
                    let remotes = repo.list_remotes()?;

                    if remotes.len() != 0 {
                        lib::utils::print_header(&format!("{:<15} {:<}", "Remote", "URL"));

                        for remote in remotes {
                            println!("{:<15} {}", remote.name, remote.url);
                        }
                    }
                }
            }
        }
        Command::Install {
            remote_name,
            app_ref,
        } => {
            repo.install_app(&remote_name, &app_ref)?;
        }
        Command::List => {
            let apps = repo.list_installed_apps()?;

            if apps.len() != 0 {
                lib::utils::print_header(&format!(
                    "{:<45} {:<15} {}",
                    "Application",
                    "Remote",
                    "Commit"
                ));

                for app in apps {
                    println!(
                        "{:<45} {:<15} {}",
                        app.as_refspec(),
                        app.remote_name,
                        app.commit.get(0..10).unwrap()
                    );
                }
            }
        }
        Command::Remove { app_ref } => {
            repo.remove_app(&app_ref)?;
        }
        Command::Update { app_ref } => {
            repo.update_app(&app_ref)?;
        }
        Command::Run { app_ref, cmd } => {
            /* FIXME: We can't simply use the obvious:
             *
             * #[structopt(required = false)]
             *
             * This is due to the following issue:
             * https://github.com/rust-lang/rust/issues/34981
             *
             * I don't really want to depend on a nightly compiler just for
             * that, though.
             */
            if cmd.len() == 0 {
                bail!("The following required arguments were not provided:\n    <cmd>")
            }

            let app = repo.get_installed_app(&app_ref)?;
            let app_path = repo.get_app_checkout_path(&app);

            let sandbox = lib::bwrap::Sandbox::new(app.id, app_path);
            sandbox.run(&cmd)?;
        }
    };

    Ok(())
}
